#include <stdio.h>

int main(void)
{
    float x,y,z; //x,y are floating point numbers
    printf("Input first floating point number: \n");
    scanf("%f",&x);
    printf("Input second floating point number: \n");
    scanf("%f",&y);
    z=x*y;//z is the answer of the multiplication of the two floating point numbers
    printf("Answer is: %f",z);

    return 0;
}
